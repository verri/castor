#include "memberwidget.h"
#include "ui_memberwidget.h"

#include <QMessageBox>
#include <QItemSelection>

#include "memberform.h"
#include "memberdialog.h"

MemberWidget::MemberWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MemberWidget)
{
    ui->setupUi(this);

    this->setEnabled(false);

    connect(ui->tableView, &QTableView::doubleClicked, this, &MemberWidget::editMemberByIndex);
    //connect(ui->tableView, &QTableView::activated, this, &MemberWidget::editMemberByIndex);

    connect(ui->pbAdd, &QPushButton::clicked, this, &MemberWidget::addMember);
    connect(ui->pbRemove, &QPushButton::clicked, this, &MemberWidget::removeMembers);
    connect(ui->pbEdit, &QPushButton::clicked, this, &MemberWidget::editMember);
}

MemberWidget::~MemberWidget()
{
    ui->tableView->setModel(0);
    delete ui;
}

void MemberWidget::setDatabase(Database *database) {
    db = database;
    if (db) {
        this->setEnabled(true);

        ui->tableView->setModel(db->members());
        ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        selectionConnection = connect(ui->tableView->selectionModel(), &QItemSelectionModel::selectionChanged,
                                      this, &MemberWidget::checkSelectionAndEnableButtons);
        if (addAction)
            addAction->setEnabled(true);

        ui->tableView->setColumnHidden(0, true);
        ui->tableView->setColumnHidden(2, true);
        ui->tableView->setColumnHidden(3, true);
        ui->tableView->setColumnHidden(4, true);
        ui->tableView->setColumnHidden(11, true);

        ui->tableView->resizeColumnsToContents();

    } else {
        this->setEnabled(false);

        disconnect(selectionConnection);
        ui->tableView->setModel(0);
        if (addAction)
            addAction->setEnabled(false);
    }

    ui->pbEdit->setEnabled(false);
    ui->pbRemove->setEnabled(false);
}

void MemberWidget::setAddAction(QAction *action)
{
    if (!addAction) {
        addAction = action;
        connect(action, &QAction::triggered, this, &MemberWidget::addMember);
    }
}

void MemberWidget::addMember()
{
    MemberDialog dialog;

    if (dialog.exec() == QDialog::Rejected)
        return;

    Member member = dialog.member();
    if (!member.isValid()) {
        QMessageBox::warning(this, "Adicionar membro", "Erro ao adicionar membro");
        return;
    }

    db->addMember(member);
    checkSelectionAndEnableButtons(ui->tableView->selectionModel()->selection(), QItemSelection());
    ui->tableView->resizeColumnsToContents();
}

void MemberWidget::removeMembers()
{
    int button = QMessageBox::question(this, "Remover membro", "Deseja deletar este membro?", "Não", "Sim");

    if (button != QMessageBox::Accepted)
        return;

    QModelIndex index = ui->tableView->currentIndex();
    Member member = db->getMember(index);
    db->removeMember(member);
    ui->tableView->resizeColumnsToContents();
}

void MemberWidget::editMember()
{
    QModelIndex index = ui->tableView->currentIndex();
    editMemberByIndex(index);
}

void MemberWidget::editMemberByIndex(const QModelIndex &index)
{
    Member member = db->getMember(index);

    MemberDialog dialog(member, db);

    if (dialog.exec() == QDialog::Rejected)
        return;

    member = dialog.member();
    if (!member.isValid()) {
        QMessageBox::warning(this, "Atualizar membro", "Erro ao atualizar membro");
        return;
    }

    db->updateMember(member);
    checkSelectionAndEnableButtons(ui->tableView->selectionModel()->selection(), QItemSelection());
    ui->tableView->resizeColumnsToContents();
}

void MemberWidget::checkSelectionAndEnableButtons(const QItemSelection &current, const QItemSelection &)
{
    ui->pbRemove->setEnabled(!current.isEmpty());
    ui->pbEdit->setEnabled(!current.isEmpty());
}
