#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "database.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openDatabase();
    void newDatabase();
    void closeDatabase();

private:
    void setDatabase(Database *database);

    Ui::MainWindow *ui;
    Database *db;
};

#endif // MAINWINDOW_H
