#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

#include "database.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->mainToolBar->setVisible(false);
    setWindowTitle("");

    ui->actionClose->setEnabled(false);

    ui->wMembers->setAddAction(ui->actionAdd);

    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionNew, &QAction::triggered, this, &MainWindow::newDatabase);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::openDatabase);
    connect(ui->actionClose, &QAction::triggered, this, &MainWindow::closeDatabase);
}

MainWindow::~MainWindow()
{
    delete ui;
    closeDatabase();
}

void MainWindow::newDatabase() {
    QString filename = QFileDialog::getSaveFileName(this, "Nova Base IEQ", "", "Base de Dados IEQ (*.ieq *.IEQ)");
    if (filename.isEmpty())
        return;

    if (!filename.endsWith(".ieq", Qt::CaseInsensitive))
        filename.append(".ieq");

    if (QFileInfo(filename).exists())
        QFile::remove(filename);

    closeDatabase();
    setDatabase(new Database(filename));
}

void MainWindow::openDatabase() {
    QString filename = QFileDialog::getOpenFileName(this, "Abrir Base IEQ", "", "Base de Dados IEQ (*.ieq)");
    if (filename.isEmpty())
        return;

    closeDatabase();
    setDatabase(new Database(filename));
}

void MainWindow::closeDatabase() {
    setWindowTitle("");
    ui->actionClose->setEnabled(false);
    ui->wMembers->setDatabase(0);
    // TODO: set in widgets.

    if (db) delete db;
    db = 0;
}

void MainWindow::setDatabase(Database *database) {

    QFileInfo info(database->name());

    if (!database->ok()) {
        QMessageBox::critical(this, "Erro ao abrir base", "A base de dados \"" +
                              info.baseName() + "\" não pode ser aberta:\n\"" +
                              database->lastError() + "\"");
        closeDatabase();
        return;
    }

    setWindowTitle(info.baseName());
    ui->actionClose->setEnabled(true);

    db = database;
    ui->wMembers->setDatabase(db);
    // TODO: set in widgets.
}
