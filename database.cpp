#include "database.h"

#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QtDebug>

#include "member.h"

Database::Database(const QString& dbname) :
    dbname(dbname)
{
    db = QSqlDatabase::addDatabase("QSQLITE", dbname);
    db.setDatabaseName(dbname);
    if (!db.open()) {
        error = "file could not be opened";
        return;
    }

    QStringList statements;
    statements
        <<
        "CREATE TABLE IF NOT EXISTS Member ("
            "id INTEGER PRIMARY KEY, "
            "name TEXT NOT NULL, "
            "sex TEXT NOT NULL, "
            "cpf TEXT, "
            "rg TEXT, "
            "birthday TEXT, "
            "phonenumber TEXT, "
            "cellnumber TEXT, "
            "address TEXT, "
            "email TEXT, "
            "obs TEXT, "

            "CHECK (sex IN ('m', 'f'))"
        ")"
        <<
        "CREATE TABLE IF NOT EXISTS Department ("
            "id INTEGER PRIMARY KEY, "
            "name TEXT NOT NULL"
        ")"
        <<
        "CREATE TABLE IF NOT EXISTS Marriage ("
            "husband INTEGER NOT NULL, "
            "wife INTEGER NOT NULL, "

            "anniversary TEXT, "

            "PRIMARY KEY (husband, wife), "
            "FOREIGN KEY (husband) REFERENCES Member(id) ON DELETE CASCADE, "
            "FOREIGN KEY (wife) REFERENCES Member(id) ON DELETE CASCADE"
        ")"
        <<
        "CREATE TRIGGER IF NOT EXISTS TriggerCheckSex BEFORE INSERT ON Marriage "
        "WHEN "
            "(SELECT sex FROM Member WHERE id = NEW.husband) = \"f\" OR "
            "(SELECT sex FROM Member WHERE id = NEW.wife) = \"m\" "
        "BEGIN "
            "SELECT RAISE(ABORT, 'Invalid sex.'); "
        "END;"
        <<
        "PRAGMA foreign_keys = ON";

    foreach(QString statement, statements) {
        if (statement.trimmed().isEmpty())
            continue;

        QSqlQuery query(db);
        query.prepare(statement);

        qDebug() << "Exec query [[\n" << statement << "\n]]";

        if (!query.exec()) {
            error = "invalid query: " + query.lastError().text();
            db.close();
            return;
        }
    }

    QSqlQuery query = makeMembersQuery();
    if (!query.exec()) {
        error = "Invalid query member selection query: " + query.lastError().text();
        db.close();
        return;
    }

    membersModel = new QSqlQueryModel;
    membersModel->setQuery(query);

    membersModel->setHeaderData(1, Qt::Horizontal, "Nome");
    membersModel->setHeaderData(5, Qt::Horizontal, "Aniversário");
    membersModel->setHeaderData(6, Qt::Horizontal, "Telefone");
    membersModel->setHeaderData(7, Qt::Horizontal, "Celular");
    membersModel->setHeaderData(8, Qt::Horizontal, "Endereço");
    membersModel->setHeaderData(9, Qt::Horizontal, "E-mail");
    membersModel->setHeaderData(10, Qt::Horizontal, "Observações");
    membersModel->setHeaderData(12, Qt::Horizontal, "Cônjuge");
}

Database::~Database()
{
    QString connectionName = db.connectionName();
    db.close();

    delete membersModel;

    QSqlDatabase::removeDatabase(connectionName);
}

const QString& Database::name() const
{
    return dbname;
}

bool Database::ok() const
{
    return db.isOpen();
}

const QString& Database::lastError() const
{
    return error;
}

QAbstractItemModel* Database::members()
{
    return membersModel;
}

static Member memberFromModel(const QSqlQueryModel* model, int row)
{
    Member member;
    member.id = model->index(row, 0).data().toInt();
    member.name = model->index(row, 1).data().toString();
    member.sex = model->index(row, 2).data().toString();
    member.cpf = model->index(row, 3).data().toString();
    member.rg = model->index(row, 4).data().toString();
    member.birthday = QDate::fromString(model->index(row, 5).data().toString(), "dd/MM/yyyy");
    member.phonenumber = model->index(row, 6).data().toString();
    member.cellnumber = model->index(row, 7).data().toString();
    member.address = model->index(row, 8).data().toString();
    member.email = model->index(row, 9).data().toString();
    member.obs = model->index(row, 10).data().toString();
    return member;
}

Member Database::getMember(const QModelIndex& index)
{
    return memberFromModel(membersModel, index.row());
}

QVector<Member> Database::getCandidates(const Member &member)
{
    QVector<Member> candidates;

    QSqlQuery query = makeCandidatesQuery(member);
    if (!query.exec()) {
        error = "Invalid query member selection query: " + query.lastError().text();
        throw error;
    }

    QSqlQueryModel model;
    model.setQuery(query);

    for (int i = 0; model.rowCount(); ++i)
        candidates.push_back(memberFromModel(&model, i));

    return candidates;
}

void Database::addMember(Member& member)
{
    if (!member.isValid() || member.id != -1) {
        qFatal("Impossible invalid member.");
        return;
    }

    QString statement =
            QString("INSERT INTO Member(name, sex, cpf, rg, birthday, phonenumber, cellnumber, address, email, obs) "
                    "VALUES ('%1', '%2', '%3', '%4', '%5', '%6', '%7', '%8', '%9', '%10')")
            .arg(member.name).arg(member.sex).arg(member.cpf).arg(member.rg)
            .arg(member.birthday.toString("dd/MM/yyyy"))
            .arg(member.phonenumber).arg(member.cellnumber).arg(member.address)
            .arg(member.email).arg(member.obs);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    member.id = query.lastInsertId().toInt();

    query = makeMembersQuery();
    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    membersModel->setQuery(query);
}

void Database::updateMember(const Member &member)
{
    if (!member.isValid() || member.id == -1) {
        qFatal("Impossible invalid member in update.");
        return;
    }

    QString statement =
            QString("UPDATE Member SET name = '%1', sex = '%2', cpf = '%3', rg = '%4', "
                    "birthday = '%5', phonenumber = '%6', cellnumber = '%7', address = '%8', "
                    "email = '%9', obs = '%10' "
                    "WHERE id = %11")
            .arg(member.name).arg(member.sex).arg(member.cpf).arg(member.rg)
            .arg(member.birthday.toString("dd/MM/yyyy"))
            .arg(member.phonenumber).arg(member.cellnumber).arg(member.address)
            .arg(member.email).arg(member.obs).arg(member.id);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    query = makeMembersQuery();
    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    membersModel->setQuery(query);
}

void Database::removeMember(Member &member)
{
    if (!member.isValid() || member.id == -1) {
        qFatal("Impossible invalid member in update.");
        return;
    }

    QString statement = QString("DELETE FROM Member WHERE id = %1").arg(member.id);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    query = makeMembersQuery();
    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    membersModel->setQuery(query);
    member.id = -1;
}

QSqlQuery Database::makeMembersQuery()
{
    QSqlQuery query(db);

    QString statement = "SELECT I.*, S.id, S.name FROM Member I "
            "LEFT OUTER JOIN Marriage M ON I.id = M.husband OR I.id = M.wife "
            "LEFT OUTER JOIN Member S ON I.id != S.id AND (S.id = M.husband OR S.id = M.wife)";

    query.prepare(statement);

    return query;
}

QSqlQuery Database::makeCandidatesQuery(const Member &member)
{
    QSqlQuery query(db);

    QString statement = QString("SELECT * FROM (SELECT * Member I "
                                "LEFT OUTER JOIN Marriage M ON I.id = M.husband OR I.id = M.wife "
                                "WHERE I.sex != %1) WHERE husband is NULL").arg(member.sex);

    query.prepare(statement);

    return query;
}
