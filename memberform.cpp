#include "memberform.h"
#include "ui_memberform.h"

MemberForm::MemberForm(QWidget *parent, Database *db) :
    QWidget(parent),
    ui(new Ui::MemberForm),
    db(db)
{
    ui->setupUi(this);

    connect(ui->leName, &QLineEdit::editingFinished, this, &MemberForm::validate);
    connect(ui->rbFemale, &QRadioButton::clicked, this, &MemberForm::validate);
    connect(ui->rbMale, &QRadioButton::clicked, this, &MemberForm::validate);
    connect(ui->cbMarried, &QCheckBox::toggled, this, &MemberForm::toggleMarried);

    ui->wMarried->setVisible(false);
}

MemberForm::~MemberForm()
{
    delete ui;
}

void MemberForm::toggleMarried(bool isMarried)
{
    ui->wMarried->setVisible(isMarried);
    ui->wSex->setEnabled(!isMarried);
}

void MemberForm::setMember(const Member &m, Database *db)
{
    this->db = db;
    this->m.id = m.id;

    ui->leName->setText(m.name);
    ui->rbMale->setChecked(m.sex == "m");

    ui->leCPF->setText(m.cpf);
    ui->leRG->setText(m.rg);

    ui->deBirthday->setDate(m.birthday);

    ui->lePhone->setText(m.phonenumber);
    ui->leCell->setText(m.cellnumber);
    ui->leAddress->setText(m.address);
    ui->leEmail->setText(m.email);

    ui->teObs->setText(m.obs);

    QVector<Member> candidates = db->getCandidates(m);

    QVector<Member>::Iterator it = candidates.begin();
    for (; it != candidates.end(); ++it)
        ui->cbSpouse->addItem(it->name, it->id);
}

const Member& MemberForm::member()
{
    updateMember();
    return m;
}

void MemberForm::validate()
{
    updateMember();
    emit validated(m.isValid());
}

void MemberForm::updateMember()
{
    m.name = ui->leName->text();
    m.sex = ui->rbMale->isChecked() ? "m" : "f";
    m.cpf = ui->leCPF->text();
    m.rg = ui->leRG->text();

    m.birthday = ui->deBirthday->date();

    m.phonenumber = ui->lePhone->text();
    m.cellnumber = ui->leCell->text();
    m.address = ui->leAddress->text();
    m.email = ui->leEmail->text();

    m.obs = ui->teObs->toPlainText();
}
