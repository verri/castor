#ifndef MEMBERWIDGET_H
#define MEMBERWIDGET_H

#include <QWidget>

#include "database.h"

namespace Ui {
class MemberWidget;
}

class QItemSelection;

class MemberWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MemberWidget(QWidget *parent = 0);
    ~MemberWidget();

    void setDatabase(Database *database);

    void setAddAction(QAction *action);

public slots:
    void addMember();
    void removeMembers();
    void editMember();
    void editMemberByIndex(const QModelIndex &index);
    void checkSelectionAndEnableButtons(const QItemSelection &current, const QItemSelection &);

private:
    Ui::MemberWidget *ui;
    Database *db;

    QAction *addAction;

    QMetaObject::Connection selectionConnection;
};

#endif // MEMBERWIDGET_H
