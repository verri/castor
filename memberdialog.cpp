#include "memberdialog.h"
#include "ui_memberdialog.h"

#include <QPushButton>

MemberDialog::MemberDialog(const Member& m, Database *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MemberDialog)
{
    ui->setupUi(this);

    QPushButton *ok = ui->buttonBox->button(QDialogButtonBox::Ok);
    ok->setText(m.id == -1 ? "Adicionar" : "Atualizar");
    ok->setEnabled(m.id != -1);

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Cancelar");

    setWindowTitle(m.id == -1 ? "Adicionar Membro" : "Atualizar Membro");

    ui->wMemberForm->setMember(m, db);

    connect(ui->wMemberForm, &MemberForm::validated,
            this, &MemberDialog::checkValid);
}

MemberDialog::~MemberDialog()
{
    delete ui;
}

void MemberDialog::checkValid(bool valid)
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(valid);
}

const Member& MemberDialog::member()
{
    return ui->wMemberForm->member();
}
