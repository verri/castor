#ifndef MEMBER_H
#define MEMBER_H

#include <QString>
#include <QDate>

struct Member
{
    Member();
    bool isValid() const;

    int id;
    QString name, sex, cpf, rg;
    QDate birthday;
    QString phonenumber, cellnumber, address, email;
    QString obs;

    int spouseId;
};

#endif // MEMBER_H
