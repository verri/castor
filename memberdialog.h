#ifndef MEMBERDIALOG_H
#define MEMBERDIALOG_H

#include <QDialog>
#include "member.h"
#include "database.h"

namespace Ui {
class MemberDialog;
}

class MemberDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MemberDialog(const Member& m = Member(), Database *db = 0, QWidget *parent = 0);
    ~MemberDialog();

    const Member& member();

public slots:
    void checkValid(bool valid);

private:
    Ui::MemberDialog *ui;
};

#endif // MEMBERDIALOG_H
