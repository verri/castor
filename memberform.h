#ifndef MEMBERFORM_H
#define MEMBERFORM_H

#include <QWidget>
#include "member.h"
#include "database.h"

namespace Ui {
class MemberForm;
}

class MemberForm : public QWidget
{
    Q_OBJECT

public:
    explicit MemberForm(QWidget *parent = 0, Database *db = 0);
    ~MemberForm();

    void setMember(const Member& m, Database *db);
    const Member& member();

signals:
    void validated(bool valid);

public slots:
    void validate();
    void toggleMarried(bool isMarried);

private:
    void updateMember();

    Database *db;
    Ui::MemberForm *ui;
    Member m;
};

#endif // MEMBERFORM_H
