#-------------------------------------------------
#
# Project created by QtCreator 2015-08-03T14:03:50
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = castor
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    memberwidget.cpp \
    memberform.cpp \
    memberdialog.cpp \
    member.cpp

HEADERS  += mainwindow.h \
    database.h \
    memberwidget.h \
    memberform.h \
    memberdialog.h \
    member.h

FORMS    += mainwindow.ui \
    memberwidget.ui \
    memberform.ui \
    memberdialog.ui
