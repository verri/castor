#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QModelIndex>
#include <QString>

class QAbstractItemModel;
class QSqlTableModel;
class QSqlQueryModel;
class QSqlQuery;
class Member;

class Database
{
public:
    Database(const QString& dbname);
    ~Database();

    const QString& name() const;

    bool ok() const;
    const QString& lastError() const;

    QAbstractItemModel* members();

    void addMember(Member& member);
    void updateMember(const Member& member);
    void removeMember(Member& member);

    Member getMember(const QModelIndex &index);
    QVector<Member> getCandidates(const Member& member);

private:
    QSqlQuery makeMembersQuery();
    QSqlQuery makeCandidatesQuery(const Member& member);

    QSqlDatabase db;

    QString dbname;
    QString error;

    QSqlQueryModel *membersModel;
};

#endif // DATABASE_H
