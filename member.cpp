#include "member.h"

Member::Member() : id(-1), spouseId(-1)
{

}

bool Member::isValid() const
{
    return !name.isEmpty() &&
           (sex == "f" || sex == "m");
}

